from dolfin import *
from input_output import load_mesh
import numpy as np
import collections
import sys
import os

def mark_boundaries(mesh):
    # detects colinear boundary facets for boundary coarsening/refinement.
    # mesh is the mesh to be adapted, tol is the angle tolerance in degrees,
    # i.e. if two facets have an angle between them less than tol, they are
    # considered as colinear.
    # This function assigns each colinear group of boundary facets a different
    # colour. The colouring is stored in a facet MeshFunction which is returned as an output.
    #
    # The colouring is performed by assigning a new colour to an uncoloured boundary facet
    # and by checking if the angle between the facet normal and the normals of
    # the facet neighbours is below the prescribed tolerance. This operation is repeated
    # for each of the neighbour facets until no colinear neighbours are detected.
    # A new colour is then assigned to the next uncoloured facet and the operation
    # is repeated until all facets are coloured.

    dim = mesh.topology().dim()

    # compute boundary mesh and initialise connectivity
    bmesh = BoundaryMesh(mesh, 'exterior')
    bmesh.init(dim-2, dim-1)
    bmesh.init(dim-1, dim-2)

    # compute connectivity from facets to edges/vertices
    # and vice versa
    bmt = bmesh.topology()
    onetwo = bmt(dim-2,dim-1)
    twoone = bmt(dim-1,dim-2)

    # compute the index of the adjacent facets
    def facetmap(facet_ind):
        return [elem[elem!=facet_ind][0] for elem in [onetwo(item) for item in twoone(facet_ind)]]

    # initialise a boolean array to mark visited cells and the colormap array
    notvisited = np.ones((bmesh.num_cells(),), dtype=bool)
    colormap = np.zeros((bmesh.num_cells(),), dtype = 'int32')

    # first colour to use is 1, then update it when a new colour is needed.
    next_col = 1
    while True:
        # get the next uncoloured facet, if there are none, break.
        try: origin = np.argwhere(notvisited)[0][0]
        except IndexError: break
        # assign the colour
        colormap[origin] = next_col
        # find the group of the adjacent colinear facets which have not been visited yet
        nextgroup = [item for item in facetmap(origin) if notvisited[item]]
        notvisited[origin] = False
        # keep going through the adjacent facets until no more colinear facets are found
        while len(nextgroup) > 0:
            temp = []
            for nxt in nextgroup:
                # find the group of the adjacent colinear facets which have not been visited yet
                temp += [item for item in facetmap(nxt) if notvisited[item] and item not in temp]
                notvisited[nxt] = False
                colormap[nxt] = next_col
            nextgroup = [item for item in temp if notvisited[item]]

        # update next colour
        next_col += 1

    return colormap

if __name__  == "__main__":

    filepath = os.path.abspath(sys.argv[1])
    filename = filepath.split("/")[-1]
    path = filepath[:-len(filename)]

    if not os.path.exists(path + "boundary_meshes"):
        os.makedirs(path + "boundary_meshes")

    output_basename = path + "boundary_meshes/" + filename.split(".")[0]

    mesh = load_mesh(filepath)

    colormap = mark_boundaries(mesh)

    bmesh = Mesh(BoundaryMesh(mesh, "exterior"))
    colors = MeshFunction("size_t", bmesh, mesh.topology().dim()-1)
    colors.array()[:] = colormap

    surfaces, counts = np.unique(colormap, return_counts=True)

    indices = np.argsort(counts)[::-1]

    surfaces = surfaces[indices]

    # NOTE: assuming the outer boundary surface has the largest amount of facets
    submesh = SubMesh(bmesh, colors, surfaces[0])
    outfilename = output_basename + "-outer."

    f = XDMFFile(outfilename + "xdmf")
    f.write(submesh)
    f.close()

    for i,surface in enumerate(surfaces[1:]):
        submesh = SubMesh(bmesh, colors, surface)
        outfilename = output_basename + "-hole%d." % i
        f = XDMFFile(outfilename + "xdmf")
        f.write(submesh)
        f.close()
