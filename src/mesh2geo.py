from dolfin import *
from input_output import load_mesh
import numpy as np
import sys

def mesh2geo(filename, save=False):
    mesh = load_mesh(filename)
    mesh.init()

    X = mesh.coordinates()

    dim = mesh.topology().dim()

    bbcoor = np.vstack([np.min(X,axis=0), np.max(X,axis=0)]).T

    clength = np.median([c.h() for c in cells(mesh)])

    def is_a_in_x(a, x):
        for i in range(len(x) - len(a) + 1):
            if a == x[i:i+len(a)]: return 1
        return -1

    lines = []
    for v in vertices(mesh):
        lines.append("Point(%d) = {%e, %e, %e};\n" % (v.index()+1, *(v.point().array())))

    if dim == 2:
        for e in edges(mesh):
            lines.append("Line(%d) = {%d, %d};\n" % (e.index()+1, *(e.entities(0)+1)))

        for c in cells(mesh):

            order = c.entities(0)
            signs = np.array([is_a_in_x(list(e.entities(0)), list(order)) for e in edges(c)]).astype(np.int)
            
            edge_list = (c.entities(1)+1).astype(np.int)

            edge_list = signs*edge_list

            lines.append("Curve Loop(%d) = {%d, %d, %d};\n" % (c.index()+1, *edge_list))
            lines.append("Plane Surface(%d) = {%d};\n" % (c.index()+1, c.index()+1))

    elif dim == 1:
        cell_vertices = mesh.cells()
        index = None
        old_index = None
        while index != 0:
            if index == None: index = 0
            vertex_list = cell_vertices[index,:].astype(np.int)

            if index > 0:
                new_cells = list(np.unique(np.concatenate([ff.entities(1).astype(np.int) for ff in [Facet(mesh, vertex) for vertex in vertex_list]])))
                new_cells.remove(old_index)
                new_cells.remove(index)
                if vertex_list[0] == old_vertex:
                    order = vertex_list + 1
                    old_vertex = vertex_list[1]
                else:
                    order = vertex_list[::-1] + 1
                    old_vertex = vertex_list[0]

                old_index, index = index, new_cells[0]
            else:
                new_cells = list(Facet(mesh,vertex_list[1]).entities(1).astype(np.int))
                new_cells.remove(0)
                old_vertex = vertex_list[1]
                old_index, index = index, new_cells[0]

                order = vertex_list + 1

            lines.append("Line(%d) = {%d, %d};\n" % (index+1, *order))

    if save == True:
        with open(filename[:-3] + "geo", "w") as f:
            f.writelines(lines)

    return lines, bbcoor, clength

if __name__ == "__main__":
    filename = sys.argv[1]
    lines, bbcoor, clength = mesh2geo(filename, save=True)
