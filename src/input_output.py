from dolfin import *

def load_mesh(filename, mpi_comm = MPI.comm_world):
    extension = filename.split(".")[-1]
    if extension == "xml":
        return Mesh(mpi_comm, filename)
    elif extension == "xdmf":
        file = XDMFFile(mpi_comm, filename)
        mesh = Mesh(mpi_comm)
        file.read(mesh)
        file.close()
        return mesh
    elif extension == "h5":
        file = HDF5File(mpi_comm, filename, "r")
        mesh = Mesh(mpi_comm)
        file.read(mesh, "/mesh", False)
        file.close()
        return mesh
    else:
        raise ValueError("Format not supported. Formats supported: xml, xdmf, h5.")

