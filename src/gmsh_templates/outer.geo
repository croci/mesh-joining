inner_loop = newll;
Line Loop(inner_loop) = Line '*';

Transfinite Line {:} = 1;

new_point = newp;
Point(new_point+0) = {xmin, ymin, 0};
Point(new_point+1) = {xmax, ymin, 0};
Point(new_point+2) = {xmax, ymax, 0};
Point(new_point+3) = {xmin, ymax, 0};

new_line = newl;
Line(new_line + 0) = {new_point, new_point + 1};
Line(new_line + 1) = {new_point+1, new_point+2};
Line(new_line + 2) = {new_point+2, new_point+3};
Line(new_line + 3) = {new_point+3, new_point};

// Outer boundary
outer_loop = newll;
Curve Loop(outer_loop) = {new_line, new_line+1, new_line+2, new_line+3};

// Box surface
outer_surf = news;
Plane Surface(outer_surf) = {outer_loop, inner_loop};
