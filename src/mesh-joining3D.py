from mesh2geo import mesh2geo
import numpy as np
import meshio
import sys
import os
import subprocess
import time

gmsh_executable = "/home/croci/Software/gmsh-git-Linux64/bin/gmsh"
mesh_joining_src = "/home/croci/Python/mesh-joining/src"

lmbda_percent = np.float(sys.argv[2])

filepath = os.path.abspath(sys.argv[1])
filename = filepath.split("/")[-1]
path = filepath[:-len(filename)]

boundary_meshes_path = path + "boundary_meshes/"

xdmffiles = [f for f in os.listdir(boundary_meshes_path) if os.path.isfile(os.path.join(boundary_meshes_path, f)) and f.split(".")[-1] == "xdmf"]
xdmffiles.sort()

holes = False
if len(xdmffiles) == 0:
    raise RuntimeError("No xdmf boundary mesh files found!")
elif len(xdmffiles) == 1:
    print("Found inner mesh with no holes")
else:
    print("Found inner mesh with %d holes" % (len(xdmffiles)-1))
    holes = True

if not os.path.exists(path + "gmsh_output"):
    os.makedirs(path + "gmsh_output")

output_basename = path + "gmsh_output/" + filename.split(".")[0]

logfile_name = output_basename + '-log.log'
errorlog_file_name = output_basename + '-errorlog.log'
if os.path.exists(logfile_name):
    os.remove(logfile_name)
if os.path.exists(errorlog_file_name):
    os.remove(errorlog_file_name)

log = open(logfile_name, 'a')
errlog = open(errorlog_file_name, 'a')

####################################################################

# First, mesh all the holes
print("Meshing the holes...")
if holes == True:
    with open(mesh_joining_src + "/gmsh_templates/hole3D.geo", "r") as f: 
        template_lines = f.readlines()
        for i in range(len(xdmffiles)-1):
            geo_lines, _, clength = mesh2geo(boundary_meshes_path + xdmffiles[i])

            lines = template_lines.copy()
            lines = ["clength = %e;" % clength] + lines
            
            geo_lines += lines

            geo_file_name = path + "gmsh_output/" + xdmffiles[i][:-4] + "geo"
            with open(geo_file_name, "w") as g:
                g.writelines(geo_lines)

            command = gmsh_executable + " " + geo_file_name + " -3 -bin &"
            p = subprocess.Popen(['/bin/bash', '-i', '-c', command], stdout=log, stderr=errlog)
            p.wait()
            sys.stdout.flush()
            while not os.path.exists(path + "gmsh_output/" + xdmffiles[i][:-4] + "msh"):
                time.sleep(1)

####################################################################

print("Getting outer surface...")
geo_lines, bbcoor, _ = mesh2geo(boundary_meshes_path + xdmffiles[-1])

new_bbox = bbcoor.copy()
diameter = new_bbox[:,1] - new_bbox[:,0]

new_bbox[:,0] = new_bbox[:,0] - lmbda_percent*diameter
new_bbox[:,1] = new_bbox[:,1] + lmbda_percent*diameter
delta_z = new_bbox[2,1]-new_bbox[2,0]

# Then mesh the box with the outer boundary surface
print("Meshing the outer portion of the mesh...")
with open(mesh_joining_src + "/gmsh_templates/outer3D.geo", "r") as f:
    lines = f.readlines()
    for i in range(len(lines)):
        if lines[i][:5] == "Point":
            lines[i] = lines[i].replace("xmin", str(new_bbox[0,0]))
            lines[i] = lines[i].replace("xmax", str(new_bbox[0,1]))
            lines[i] = lines[i].replace("ymin", str(new_bbox[1,0]))
            lines[i] = lines[i].replace("ymax", str(new_bbox[1,1]))
            lines[i] = lines[i].replace("zmin", str(new_bbox[2,0]))
        if lines[i][:3] == "v[]":
            lines[i] = lines[i].replace("zmax-zmin", str(delta_z))

    geo_lines += lines
    geo_file_name = path + "gmsh_output/" + xdmffiles[-1][:-4] + "geo"
    with open(geo_file_name, "w") as g:
        g.writelines(geo_lines)

    command = gmsh_executable + " " + geo_file_name + " -3 -bin &"
    p = subprocess.Popen(['/bin/bash', '-i', '-c', command], stdout=log, stderr=errlog)
    p.wait()

sys.stdout.flush()

# Convert original mesh to msh file as well
m = meshio.read(filepath)
meshio.write(output_basename + ".msh", m)

while not os.path.exists(path + "gmsh_output/" + xdmffiles[-1][:-4] + "msh"):
    time.sleep(1)

####################################################################

# Now, time to merge everything
print("Time to merge everything...")

gmsh_output_path = path + "gmsh_output/"
mshfiles = [f for f in os.listdir(gmsh_output_path) if os.path.isfile(os.path.join(gmsh_output_path, f)) and f.split(".")[-1] == "msh"]
mshfiles.sort()

lines = []
for i in range(len(mshfiles)):
    lines.append('Merge "%s";\n' % (gmsh_output_path + mshfiles[i]))  
    #lines.append('Physical Volume(%d) = {%d};\n' % (i,i))  

lines.append("Physical Volume(newv) = {Volume '*'};\n")# % len(mshfiles))
lines.append("Coherence Mesh;\n")
lines.append("RenumberMeshNodes;\n")
lines.append("RenumberMeshElements;\n")

geo_file_name = output_basename + "-merged.geo"
with open(geo_file_name, "w") as g:
    g.writelines(lines)

command = gmsh_executable + " " + geo_file_name + " -3 -bin &"
p = subprocess.Popen(['/bin/bash', '-i', '-c', command], stdout=log, stderr=errlog)
p.wait()

sys.stdout.flush()

while not os.path.exists(output_basename + "-merged.msh"):
    time.sleep(1)

# Convert final mesh to xdmf
t = 15
oneday = 24*60*60
while t < 1.5*oneday:
    try: m = meshio.read(output_basename + "-merged.msh"); break;
    except AssertionError:
        print("Could not read meshfile with meshio. Trying again in %.2f mins" % (t/60))
        time.sleep(t)
        t *= 2

if t > 1.5*oneday:
    raise RuntimeError("Meshio could not read merged mesh file")

meshio.write(output_basename + "-merged.xdmf", m)

####################################################################

errlog.close()
with open(errorlog_file_name, 'r') as errlog:
    lines = errlog.readlines()
    for line in lines:
        if line[0] != "[":
            print(line)

sys.stdout.flush()
