shell_loop = newsl;
Surface Loop(shell_loop) = {Surface '*'};

Volume(1) = {shell_loop};

Transfinite Line {:} = 1;
Transfinite Surface {:} = 1;

// clength = 0.1;
Field[1] = Distance;
Field[1].NodesList = {Point '*'};
Field[2] = MathEval;
Field[2].F = Sprintf("((10*F1)^2 + 1)*%f", clength);
Background Field = 2;

Mesh.CharacteristicLengthFromPoints = 0;
Mesh.CharacteristicLengthFromCurvature = 0;
Mesh.CharacteristicLengthExtendFromBoundary = 0;
