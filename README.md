Instructions
=============

* Step 1 - Convert the original mesh in xml using meshio-convert to obtain mesh.xml
* Step 2 - Run extract_bndry.py /path/to/mesh.xml
* Step 3 - Run mesh-joining.py /path/to/mesh.xml lmbda_percent in 2D or mesh-joining3D.py /path/to/mesh.xml lmbda_percent in 3D. The output file mesh-merged.xml will be into the gmsh_output folder

Dependencies:
=============
* Dolfin
* meshio
* numpy

Contact:
=========
* Matteo Croci matteo.croci@maths.ox.ac.uk
