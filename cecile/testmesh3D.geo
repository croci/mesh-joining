SetFactory("OpenCASCADE");
Merge "testmesh3D.step";
Mesh.CharacteristicLengthMax = 5e-3;

h=1e-2;

// Box volume
// 1 : Build the surface corresponding to one side of the box
// Equivalent to the 2D example, with z = -1e-2
Point(1000) = {-1e-2, -1e-2, -1e-2, h};
Point(2000) = {1e-2, -1e-2, -1e-2, h};
Point(3000) = {1e-2, 1e-2, -1e-2, h};
Point(4000) = {-1e-2, 1e-2, -1e-2, h};

Line(170) = {1000, 2000};
Line(180) = {2000, 3000};
Line(190) = {3000, 4000};
Line(200) = {4000, 1000};

Curve Loop(900) = {190, 200, 170, 180};
Plane Surface(900) = {900};

// 2 : Extrude the built surface to build the 3D box
Extrude {0,0,2e-2} { Surface{900}; }

Transfinite Line {:} = 1;
Transfinite Surface {:} = 1;

// Volume{2} is the box volume, including the inner mesh
// Volume{1} is the inner mesh (input) volume
// Output : a new holed volume = Volume{2} - Volume{1}
BooleanDifference{Volume{2}; Delete;}{Volume{1};Delete;}
