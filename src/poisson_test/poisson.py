from dolfin import *
import sys

#mesh = Mesh("/scratch/croci/test.xml")

mesh = Mesh("./test.xml")

#mesh = Mesh()
#f = XDMFFile("/scratch/croci/augmented_meshes/colin_RB_superfine/gmsh_output/colin_RB_superfine-merged.xdmf")
#f.read(mesh)

V = FunctionSpace(mesh, 'CG', 1)

u = TrialFunction(V)
v = TestFunction(V)

bcs = DirichletBC(V, Constant(0.0), 'on_boundary')

form = inner(grad(u), grad(v))*dx
rhs  = Constant(1.0)*v*dx

sol = Function(V)

solve(form == rhs, sol, bcs, solver_parameters={"linear_solver": "cg", "preconditioner" : "hypre_amg"})

File("/scratch/croci/sol.pvd") << sol
#File("sol.pvd") << sol

#import matplotlib.pyplot as plt
#plot(sol)
#plt.show()
