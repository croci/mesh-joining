inner_loop = newll;
inner_surf = news;
Line Loop(inner_loop) = Line '*';
Plane Surface(inner_surf) = {inner_loop};

Transfinite Line {:} = 1;

// clength = 0.1;
Field[1] = Distance;
Field[1].NodesList = {Point '*'};
Field[2] = MathEval;
Field[2].F = Sprintf("((10*F1)^2 + 1)*%f", clength);
Background Field = 2;

Mesh.CharacteristicLengthFromPoints = 0;
Mesh.CharacteristicLengthFromCurvature = 0;
Mesh.CharacteristicLengthExtendFromBoundary = 0;
