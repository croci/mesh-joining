SetFactory("OpenCASCADE");
Merge "testmesh.step";
//Mesh.CharacteristicLengthMax = 5e-4;

h=1e-5;
Point(100) = {-1e-3, -1e-3, 0, h};
Point(200) = {2e-3, -1e-3, 0, h};
Point(300) = {2e-3, 2e-3, 0, h};
Point(400) = {-1e-3, 2e-3, 0, h};

Line(17) = {100, 200};
Line(18) = {200, 300};
Line(19) = {300, 400};
Line(20) = {400, 100};

// Outer boundary
Curve Loop(9) = {19, 20, 17, 18};

// Box surface
Plane Surface(9) = {9};
// We can specify that the boundary facets have to be on the output mesh
// Curve{16, 12, 13, 5, 2, 8, 9, 15} In Surface {9};
Transfinite Line {:} = 1;

// Surface{9} is the box surface, including the inner mesh
// Surface{1:8} are the inner mesh (input) surfaces
// Output : a new holed surface = Surface{9} - Surface{1:8}
BooleanDifference{Surface{9}; Delete;}{Surface{1:8};Delete;}
