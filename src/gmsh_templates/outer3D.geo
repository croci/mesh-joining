inner_loop = newsl;
Surface Loop(inner_loop) = Surface '*';

Transfinite Line {:} = 1;
Transfinite Surface {:} = 1;

// Box volume
// 1 : Build the surface corresponding to one side of the box
// Equivalent to the 2D example, with z = zmin
new_point = newp;
Point(new_point + 0) = {xmin, ymin, zmin};
Point(new_point + 1) = {xmax, ymin, zmin};
Point(new_point + 2) = {xmax, ymax, zmin};
Point(new_point + 3) = {xmin, ymax, zmin};

new_line = newl;
Line(new_line + 0) = {new_point,   new_point + 1};
Line(new_line + 1) = {new_point+1, new_point + 2};
Line(new_line + 2) = {new_point+2, new_point + 3};
Line(new_line + 3) = {new_point+3, new_point    };

face_loop = newll;
face = news;
Curve Loop(face_loop) = {new_line+0,new_line+1, new_line+2, new_line+3};
Plane Surface(face) = {face_loop};

// 2 : Extrude the built surface to build the 3D box
v[] = Extrude {0,0,zmax-zmin} { Surface{face}; };

outer_loop = newsl;
Surface Loop(outer_loop) = {face, v[0], v[2], v[3], v[4], v[5]};
Delete {Volume {v[1]};}

Volume(newv) = {outer_loop, inner_loop};
